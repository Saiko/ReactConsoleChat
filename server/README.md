# Server ConsoleChat
## Start dev server
For `Yarn`
```bash
/server$ yarn start:dev
``` 
For `Npm`
```bash
/server$ npm run start:dev
``` 
