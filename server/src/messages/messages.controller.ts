import { Controller, Body, Param, Post, Get, Delete, UseGuards } from '@nestjs/common';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from 'src/auth/auth.guard';
import { CreateMessageDTO } from './dto/createMessage.dto';
import { Message } from './messages.model';
import { MessagesService } from './messages.service';

@ApiTags('Messages')
@UseGuards(AuthGuard)
@Controller('messages')
export class MessagesController {

    constructor(private messageService: MessagesService) {}

    @ApiOperation({ summary: "Создание сообщения" })
    @ApiResponse({ status: 200, type: Message })
    @ApiHeader({
        name: "Authorization",
        required: true,
        description: 'User Bearer Token'
    })
    @Post()
    createMessage(@Body() dto: CreateMessageDTO) {
        return this.messageService.createMessage(dto);
    }

    @ApiOperation({ summary: "Получение всех сообщений" })
    @ApiResponse({ status: 200, type: [Message] })
    @Get()
    getAll() {
        return this.messageService.getAllMessages();
    }

    @ApiOperation({ summary: "Получение одного сообщения" })
    @ApiResponse({ status: 200, type: Message })
    @Get('/:id')
    get(@Param('id') id: string) {
        return this.messageService.getMessage(id);
    }

    @ApiOperation({ summary: "Удаление сообщения" })
    @ApiResponse({ status: 200, type: Message })
    @Delete('/:id')
    delete(@Param('id') id: string) {
        return this.messageService.deleteMessage(id);
    }

}
