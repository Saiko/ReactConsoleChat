import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { User } from 'src/users/users.model';
import { CreateMessageDTO } from './dto/createMessage.dto';
import { Message } from './messages.model';

@Injectable()
export class MessagesService {

    constructor(@InjectModel(Message) private messageRepository: typeof Message) {}

    async createMessage(dto: CreateMessageDTO) {
        const message = await this.messageRepository.create(dto, { include: { all: true } });
        return message;
    }

    async getAllMessages() {
        const messages = await this.messageRepository.findAll({ include: User });
        return messages;
    }

    async getMessage(id: string) {
        const message = await this.messageRepository.findByPk(id, { include: User });        
        return message;
    }

    async deleteMessage(id: string) {
        const count = await this.messageRepository.destroy({ where: { id } });
        return count;
    }

}
