import { BelongsTo, Column, DataType, ForeignKey, Model, Table } from "sequelize-typescript";
import { ApiProperty } from "@nestjs/swagger/dist"
import { User } from "src/users/users.model";


interface MessageCreationAttrs {
    content: string;
    userId: number;
}

@Table({ tableName: "messages" })
export class Message extends Model<Message, MessageCreationAttrs> {

    @ApiProperty({ example: 1, description: 'Уникальный идентификатор сообщения' })
    @Column({ type: DataType.INTEGER, autoIncrement: true, primaryKey: true })
    id: number;
    
    @ApiProperty({ example: 'Hello, World!', description: 'Контент сообщения' })
    @Column({ type: DataType.STRING, allowNull: false })
    content: string;

    @ForeignKey(() => User)
    @Column({ type: DataType.INTEGER, allowNull: false })
    userId: number;

    @BelongsTo(() => User)
    author: User;
}