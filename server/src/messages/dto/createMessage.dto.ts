import { ApiProperty } from '@nestjs/swagger';

export class CreateMessageDTO {

    @ApiProperty({ example: 'Hello, World!', description: 'Текст сообщения' })
    readonly content: string;

    @ApiProperty({ example: 1, description: 'Идентификатор автора сообщения' })
    readonly userId: number;

}