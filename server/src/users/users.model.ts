import { Column, DataType, HasMany, Model, Table } from "sequelize-typescript";
import { ApiProperty } from "@nestjs/swagger/dist"
import { Message } from "src/messages/messages.model";


interface UserCreationAttrs {
    login: string;
    password: string;
}


@Table({ tableName: "users" })
export class User extends Model<User, UserCreationAttrs> {

    @ApiProperty({ example: '1', description: 'Уникальный идентификатор пользователя' })
    @Column({ type: DataType.INTEGER, autoIncrement: true, primaryKey: true })
    id: number;
    
    @ApiProperty({ example: 'John', description: 'Логин пользователя' })
    @Column({ type: DataType.STRING, unique: true, allowNull: false })
    login: string;
    
    @ApiProperty({ example: 'JohnPassword', description: 'Пароль пользователя' })
    @Column({ type: DataType.STRING, allowNull: false })
    password: string;

    @HasMany(() => Message)
    messages: Message[]
}