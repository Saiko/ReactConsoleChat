import { Controller, Body, Post, Delete, Get } from '@nestjs/common';
import { Request, UseGuards } from '@nestjs/common/decorators';
import { ApiOperation, ApiResponse } from '@nestjs/swagger/dist';
import { ApiTags } from '@nestjs/swagger/dist/decorators';
import { AuthGuard } from 'src/auth/auth.guard';
// import { CreateUserDTO } from './dto/createUser.dto';
import { User } from './users.model';
import { UsersService } from './users.service';

@ApiTags('Users')
@Controller('users')
export class UsersController {

    constructor(private usersService: UsersService) {}

    // @ApiOperation({ summary: "Создание пользователя" })
    // @ApiResponse({ status: 200, type: User })
    // @Post()
    // create(@Body() userDTO: CreateUserDTO) {
    //     return this.usersService.createUser(userDTO);
    // }

    @ApiOperation({ summary: "Возвращение всех пользователей" })
    @ApiResponse({ status: 200, type: [User] })
    @UseGuards(AuthGuard)
    @Get()
    getAll() {
        return this.usersService.getAllUsers();
    }

    @ApiOperation({ summary: "Удаляет текущего зарегистрированного пользователя" })
    @ApiResponse({ status: 200, type: [User] })
    @UseGuards(AuthGuard)
    @Delete()
    delete(@Request() req) {
        return this.usersService.deleteUser(req.user.id);
    }

}
