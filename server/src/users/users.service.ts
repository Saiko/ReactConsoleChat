import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreateUserDTO } from './dto/createUser.dto';
import { User } from './users.model';

@Injectable()
export class UsersService {

    constructor(@InjectModel(User) private userRepository: typeof User) {}

    async createUser(dto: CreateUserDTO) {
        const user = await this.userRepository.create(dto);
        return user;
    }

    async getAllUsers() {
        const users = await this.userRepository.findAll();
        return users;
    }

    async getUserByLogin(login: string) {
        const user = await this.userRepository.findOne({ where: { login }});
        return user;
    }

    async deleteUser(id: number) {
        const count = await this.userRepository.destroy({ where: { id }});
        return count;
    }

}
