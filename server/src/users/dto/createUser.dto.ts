import { ApiProperty } from "@nestjs/swagger/dist/decorators";

export class CreateUserDTO {

    @ApiProperty({ example: 'John', description: 'Логин пользователя' })
    readonly login: string;

    @ApiProperty({ example: 'JohnPassword', description: 'Пароль пользователя' })
    readonly password: string;
    
}