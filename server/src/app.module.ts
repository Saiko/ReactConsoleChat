import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { SequelizeModule } from "@nestjs/sequelize";
import { User } from "./users/users.model";
import { UsersModule } from './users/users.module';
import { MessagesModule } from './messages/messages.module';
import { Message } from "./messages/messages.model";
import { AuthModule } from './auth/auth.module';
import { JwtService } from "@nestjs/jwt";


@Module({
    controllers: [
        
    ],
    providers: [
    ],
    imports: [
        ConfigModule.forRoot({
            envFilePath: `.${process.env.NODE_ENV}.env`,
            isGlobal: true
        }),
        SequelizeModule.forRoot({
            dialect: 'postgres',
            host: process.env.POSTGRES_HOST,
            port: Number(process.env.POSTGRES_PORT),
            database: process.env.POSTGRES_DATABASE,
            username: process.env.POSTGRES_USERNAME,
            password: process.env.POSTGRES_PASSWORD,
            autoLoadModels: true,
            models: [User, Message]
        }),
        AuthModule,
        UsersModule,
        MessagesModule
    ]
})
export class AppModule {}