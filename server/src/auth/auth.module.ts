import { Module } from '@nestjs/common';
import { forwardRef } from '@nestjs/common/utils';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';



@Module({
  controllers: [AuthController],
  providers: [AuthService],
  imports: [
    JwtModule.registerAsync({
      useFactory: _ => {
        return {
          secret: process.env.JWT_SECRET,
          signOptions: {
            expiresIn: '24h',
          },
        }
      }
    }),
    forwardRef(() => UsersModule)
  ],
  exports: [
    JwtModule,
    AuthService
  ]
})
export class AuthModule {}
