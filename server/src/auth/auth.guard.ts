import { CanActivate, ExecutionContext } from '@nestjs/common';
import { Injectable } from '@nestjs/common/decorators';
import { UnauthorizedException } from '@nestjs/common/exceptions';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { Observable } from "rxjs";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private jwtService: JwtService,
        private configService: ConfigService) {}

    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        const req = context.switchToHttp().getRequest();
        try {
            const header = req.headers.authorization;
            if (!header) {
                throw new UnauthorizedException({ message: "Пользователь не авторизирован" });
            }
            
            const [bearer, token] = header.split(' ');

            if (bearer !== 'Bearer' || !token) {
                throw new UnauthorizedException({ message: "Пользователь не авторизирован" });
            }
            
            
            const user = this.jwtService.verify(token, {
                secret: this.configService.get('JWT_SECRET')
            });
            req.user = user;
            
            return true;

        } catch (e) {
            throw new UnauthorizedException({ message: "Пользователь не авторизирован" });      
        }
    }
}