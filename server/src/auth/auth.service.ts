import { Injectable } from '@nestjs/common';
import { HttpStatus } from '@nestjs/common/enums';
import { HttpException, UnauthorizedException } from '@nestjs/common/exceptions';
import { JwtService } from '@nestjs/jwt/dist';
import { CreateUserDTO } from 'src/users/dto/createUser.dto';
import { UsersService } from 'src/users/users.service';
import * as bcrypt from 'bcrypt';
import { User } from 'src/users/users.model';

@Injectable()
export class AuthService {

    constructor(
        private userService: UsersService,
        private jwtService: JwtService
    ) {}

    async login(dto: CreateUserDTO) {
        const user = await this.validateUser(dto);
        return {
            token: this.generateToken(user)
        };
    }

    async register(dto: CreateUserDTO) {
        const candidate = await this.userService.getUserByLogin(dto.login);
        if (candidate) {
            throw new HttpException('Пользователь с таким логином уже существует', HttpStatus.BAD_REQUEST);
        }
        const hashPassword = await bcrypt.hash(dto.password, 5);
        const user = await this.userService.createUser({ ...dto, password: hashPassword });
        return {
            token: this.generateToken(user)
        };
    }

    private generateToken(user: User) {
        const payload = { login: user.login, id: user.id };
        return this.jwtService.sign(payload);
    }

    private async validateUser(dto: CreateUserDTO) {
        const user = await this.userService.getUserByLogin(dto.login);
        if (!user) {
            throw new UnauthorizedException('Неверный логин или пароль');
        }

        const comparedPassword = await bcrypt.compare(dto.password, user.password);
        if (!comparedPassword) {
            throw new UnauthorizedException('Неверный логин или пароль');
        }
        
        return user;
    }

}
