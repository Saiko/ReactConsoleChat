import { Controller, Post, Get, Body, Request } from '@nestjs/common';
import { UseGuards } from '@nestjs/common/decorators';
import { ApiTags } from '@nestjs/swagger';
import { CreateUserDTO } from 'src/users/dto/createUser.dto';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';

@ApiTags('Authorization')
@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService) {}

    @Post('login')
    login(@Body() dto: CreateUserDTO) {
        return this.authService.login(dto);
    }

    @Post('register') 
    register(@Body() dto: CreateUserDTO) {
        return this.authService.register(dto);
    }

    @Get('check')
    @UseGuards(AuthGuard)
    check(@Request() req) {
        return { ok: true };
    }

}
