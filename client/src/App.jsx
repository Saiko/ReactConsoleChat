import { AppRouter } from "./components/AppRouter";
import { ScreenEffect } from "./components/ScreenEffect";
import { Provider } from 'react-redux';
import store from './store'

function App() {
  return (<>
    <Provider store={store}>
      <ScreenEffect></ScreenEffect>
      <AppRouter></AppRouter>
    </Provider>
  </>
  );
}

export default App;
