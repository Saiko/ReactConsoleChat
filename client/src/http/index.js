import axios from 'axios';

const $host = axios.create({
    baseURL: process.env.REACT_APP_API_URL
});
const $authhost = axios.create({
    baseURL: process.env.REACT_APP_API_URL
});

$authhost.interceptors.request.use(authInterceptor);

function authInterceptor(config) {
    config.headers.Authorization = `Bearer ${localStorage.getItem('token')}`;
    return config;
}


export {
    $host,
    $authhost
};