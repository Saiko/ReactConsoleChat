import { $host, $authhost } from './index';
import jwtDecode from 'jwt-decode';


export function tokenDecode(token) {
    if (!token) token = localStorage.getItem('token');
    else localStorage.setItem('token', token);
    const userData = jwtDecode(token);
    return userData
}

export const registration = async (login, password) => {
    const res = await $host.post('auth/register', { login, password });
    return tokenDecode(res.data.token);
};

export const login = async (login, password) => {
    const res = await $host.post('auth/login', { login, password });
    return tokenDecode(res.data.token);
};

export const check = async () => {
    await $authhost.get('auth/check');
    return tokenDecode();
}