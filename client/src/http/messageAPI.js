import { $authhost } from './index';

export const getAllMessage = async () => {
    const res = await $authhost.get('messages');
    return res.data;
};

export const sendMessage = async (content, userId) => {
    const res = await $authhost.post('messages', { content, userId });
    return res.data;
};

export const deleteMessage = async (messageId) => {
    const res = await $authhost.delete(`messages/${messageId}`);
    return res.data;
};