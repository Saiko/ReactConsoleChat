import './Login.css';
import CommonButton from '../../components/UI/Buttons/CommonButton';
import CommonInput from '../../components/UI/Inputs/CommonInput';
import { login } from '../../http/userAPI';
import { setUserData } from '../../store/userSlice';
import { routes } from '../../routes';
import { ScreenLoading } from '../../components/ScreenLoading';

import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

export function LoginWindow() {
    const dispatch = useDispatch();
    const nav = useNavigate();

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);    

    async function signIn() {
        setLoading(true);
        login(username, password)
        .then((res) => {
            dispatch(setUserData(res));
            nav(routes.ROUTE_CHAT);
        })
        .finally(() => setLoading(false));
    }

    return <>
        {loading && <ScreenLoading/>}
        <div className="authBlock">
            <h2 style={{
                textAlign: "center",
                paddingBottom: "10px"
            }}>Login</h2>
            <div className="inputsBlock">
                <CommonInput
                    placeholder="Username"
                    style={{ marginRight: 5 }}
                    onChange={e => setUsername(e.target.value)}
                    value={username}
                />
                <CommonInput 
                    type="password"
                    placeholder="Password"
                    onChange={e => setPassword(e.target.value)}
                    value={password}
                />
            </div>
            <CommonButton onClick={signIn} type="submit" placeholder="Password" key="authSend">Login</CommonButton>
        </div>
    </>
}