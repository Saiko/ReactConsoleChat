import { setIsAuth } from '../../store/userSlice';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { routes } from '../../routes';
import { useEffect } from 'react';

export function LogoutWindow() {
    const nav = useNavigate();
    const dispatch = useDispatch();

    useEffect(() => {
        localStorage.removeItem('token');
        dispatch(setIsAuth(false));
        nav(routes.ROUTE_ABOUT);
    }, [dispatch, nav]);

    return null;
}