import './Register.css';
import CommonButton from '../../components/UI/Buttons/CommonButton';
import CommonInput from '../../components/UI/Inputs/CommonInput';
import { registration } from '../../http/userAPI';
import { setUserData } from '../../store/userSlice';

import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { routes } from '../../routes';

export function RegisterWindow() {
    const dispatch = useDispatch();
    const nav = useNavigate();

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    async function signIn() {
        registration(username, password).then((res) => {
            dispatch(setUserData(res));
            nav(routes.ROUTE_CHAT);
        });
    }

    return <>
        <div className="authBlock">
            <h2 style={{
                textAlign: "center",
                paddingBottom: "10px"
            }}>Registration</h2>
            <div className="inputsBlock">
                <CommonInput
                    placeholder="Username"
                    style={{ marginRight: 5 }}
                    onChange={e => setUsername(e.target.value)}
                    value={username}
                />
                <CommonInput 
                    type="password"
                    placeholder="Password"
                    onChange={e => setPassword(e.target.value)}
                    value={password}
                />
            </div>
            <CommonButton onClick={signIn} type="submit" placeholder="Password" key="authSend">Login</CommonButton>
        </div>
    </>
}