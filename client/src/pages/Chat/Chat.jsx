import { React, useState } from 'react';
import './Chat.css';
import CommonButton from '../../components/UI/Buttons/CommonButton';
import CommonInput from '../../components/UI/Inputs/CommonInput';
import { Message } from '../../components/Message';
import { useSelector } from 'react-redux';
import { getAllMessage, sendMessage } from '../../http/messageAPI';
import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { routes } from '../../routes';
import { useRef } from 'react';

export function ChatWindow() {
    const { userData, isAuth } = useSelector(state => state.user);
    const chatRef = useRef();

    const [messages, setMessages] = useState([]);
    const [message, setMessage] = useState('');

    useEffect(() => {

        if (isAuth) getAllMessage().then(loadedMessages => {
            setMessages([...loadedMessages]);
            
            // да.... Ночью не соображаю. По крайней мере, это работает :\
            setTimeout(() => {
                scrollToBottom(chatRef.current)
            })
        })

        return () => {
            setMessages([])
        }
    }, [isAuth])    

    useEffect(() => {
        let ref = chatRef.current;
        if (ref) {
            if (ref.scrollHeight - ref.clientHeight - ref.scrollTop < 300) scrollToBottom(ref);
        }
    }, [messages])


    function scrollToBottom(el) {
        if (!el) return;
        el.scrollTo({
            top: el.scrollHeight,
            behavior: 'smooth'
        });
    }

    async function createMessage() {
        const messageContent = message.trim();
        if (!messageContent) return;

        let messageData = await sendMessage(messageContent, userData.id);

        // Костыль. Сервис MessageService не хочет в инклюд юзера при создании инстанса :\
        messageData = {
            author: userData,
            ...messageData
        }

        setMessages([...messages, messageData]);
        setMessage('');

    }


    return (<div className="interface">
        <header>
            <Link to={routes.ROUTE_LOGOUT}><CommonButton>Logout</CommonButton></Link>
        </header>
        <div className="chat" ref={chatRef}>
            {messages.map(message => (
                <Message message={message} key={message.id}></Message>
            ))}
        </div>
        <footer>
            <CommonInput onKeyDown={e=> e.key.toLowerCase() === 'enter' && createMessage()} onChange={e=>setMessage(e.target.value)} style={{width: '100%'}} value={message}/>
            <CommonButton onClick={createMessage}>Send</CommonButton>
        </footer>
    </div>)
}