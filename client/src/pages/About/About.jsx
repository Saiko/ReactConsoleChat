import { Link } from 'react-router-dom';
import Button from '../../components/UI/Buttons/CommonButton';
import { routes } from '../../routes';

export function AboutWindow() {
    return <>
        <h1 style={{
            textAlign: "center",
            paddingBottom: "10px"
        }}>ConsoleChat v3</h1>
        <div style={{
            display: "flex",
            justifyContent: "space-evenly",
        }}>
            <Link to={routes.ROUTE_REGISTRATION}><Button style={{width: 150}}>Registration</Button></Link>
            <Link to={routes.ROUTE_LOGIN}><Button style={{width: 150}}>Login</Button></Link>
            <Link to={routes.ROUTE_CHAT}><Button style={{width: 150}}>Chat</Button></Link>
            <Button style={{width: 150}}>
                Download App (InDev) 
            </Button>
        </div>
        <footer style={{
            display: "flex",
            alignItems: "start",
            flexDirection: "column",
            position: "absolute",
            left: "0",
            bottom: "0"
        }}>
            <div>Developed by <a href="https://codeberg.org/Saiko">@Saiko</a></div>
            <img style={{ height: "24px", left: 0 }} src="https://skillicons.dev/icons?i=nestjs,react,redux,postgresql" alt="" />
        </footer>
    </>
}