import classes from './CommonButton.module.css';

export default function Button({children, ...props}) {
    return (<button className={classes.commonButton} {...props}>{children}</button>)
}
