import React from 'react';
import s from './CommonInput.module.css';

export default function CommonInput(props) {
    return (
        <input className={s.commonInput} {...props}/>
    );
}