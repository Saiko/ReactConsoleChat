import classes from './index.module.css';

export function Message({message = {}}) {

    return (<div className={classes.messageBlock}>
        <div className="usernameBlock">
            &lt;<span className={ message.author.isAdmin ? classes.UsernameAdmin : classes.Username }>{message.author.login}</span>&gt;
        </div>
        <div className="message">{ message.content }</div>
    </div>)

}