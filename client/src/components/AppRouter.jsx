import { Routes, Route } from 'react-router-dom';
import { privateRoutes, publicRoutes } from '../routes';
import ProtectRoute from './ProtectRoute';

export const AppRouter = () => {

    return (
    <Routes>
      
        {publicRoutes.map(route => 
            <Route path={route.path} exact={route.exact} element={<route.component/>} key={route.path}/>
        )}
  
        {privateRoutes.map(route => 
            <Route path={route.path} exact={route.exact} element={
                <ProtectRoute><route.component/></ProtectRoute>
            } key={route.path}/>
        )}

    </Routes>
    )
}