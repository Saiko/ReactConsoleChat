import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';
import { check } from '../http/userAPI';
import { setIsAuth, setUserData } from '../store/userSlice';

export default function ProtectRoute({ children }) {
    const dispatch = useDispatch();
    const { isAuth } = useSelector(state => state.user);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        check()
        .then((authData) => {
            dispatch(setIsAuth(!!authData));
            if (authData) dispatch(setUserData(authData))
        })
        .finally(_=> setLoading(false))
        .catch(() => {});
    }, [dispatch]);

    return (
        !loading ? (
            isAuth ? (
                children
            ) : (
                <Navigate to={{ pathname: '/login' }} replace/>
            )
        ) : (
            <></>
        )
    );
}