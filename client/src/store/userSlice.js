import { createSlice } from "@reduxjs/toolkit";


const initialState = {
    isAuth: false,
    username: null,
    userId: null
}

export const userSlice = createSlice({
    name: "user",
    initialState,
    reducers: {
        setUserData: (state, action) => {
            state.userData = action.payload;
        },
        setIsAuth: (state, action) => {
            state.isAuth = action.payload;
        }
    }
});

export const { setUserData, setIsAuth } = userSlice.actions;