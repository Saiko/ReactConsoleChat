import { AboutWindow } from '../pages/About/About';
import { LoginWindow } from '../pages/Login/Login';
import { ChatWindow } from '../pages/Chat/Chat';
import { LogoutWindow } from '../pages/Logout/Logout';
import { RegisterWindow } from '../pages/Register/Register';

export const routes = {
    ROUTE_ABOUT: "/",
    ROUTE_CHAT: "/chat",
    ROUTE_LOGIN: "/login",
    ROUTE_LOGOUT: "/logout",
    ROUTE_REGISTRATION: "/registration"
}

export const publicRoutes = [
    { path: routes.ROUTE_ABOUT,         component: AboutWindow,       exact: true },
    { path: routes.ROUTE_LOGIN,         component: LoginWindow,       exact: true },
    { path: routes.ROUTE_LOGOUT,        component: LogoutWindow,      exact: true },
    { path: routes.ROUTE_REGISTRATION,  component: RegisterWindow,    exact: true }
];

export const privateRoutes = [
    { path: routes.ROUTE_CHAT,          component: ChatWindow,        exact: true }
];